
#include "LogicBoard.h"
#include <vector>
#include <algorithm>
#include <iostream>

int const AI_PLAYER = 2;
int const HUMAN_PLAYER = 1;
int const NO_MARK = 0;
int const TIE = -1;

int const MAX_PLAYER = 2;
int const MIN_PLAYER = 1;

enum {
	EASY = 1,
	MEDIUM = 2,
	HARD = 3,
	IMPOSSIBLE = 4
};

class Move {
public:
	Move() {};
	Move(int Score) : score(Score) {};

	int x;
	int y;
	int score;
};

class MoveNode {
public:
	MoveNode() {};
	MoveNode(int Score) : score(Score) {};

	int x;
	int y;
	int score;
	int depth = 0;
	MoveNode *child;
	MoveNode *parent;
};

class PlayerAI {
public:

	void init();
	Move placeMark(LogicBoard *logicBoard);
	Move miniMax(LogicBoard *logicBoard, int depth, int player);
	Move alphaBeta(LogicBoard *logicBoard, int depth, int alpha, int beta, int Player);
	int _difficultyLevel = 0;

};


//*******************************************************************************************************************************

//*******************************************************************************************************************************

int nodeCounter = 0;

void PlayerAI::init() {

}

unsigned long long factorion(int n) {
	for (int i = n - 1; i >= 1; i--) {
		n = n * i;
	}
	return n;
}

Move PlayerAI::placeMark(LogicBoard *logicBoard) {
	nodeCounter = 0;
	Move bestMove;
	int depth;
	switch (_difficultyLevel) {
	case EASY:
		depth = 2;
		break;
	case MEDIUM:
		depth = logicBoard->checkSize();
		break;
	case HARD:
		depth = (logicBoard->checkSize()*logicBoard->checkSize())/2;
		break;
	case IMPOSSIBLE:
		depth = logicBoard->checkSize()*logicBoard->checkSize();
		break;
	}
	bestMove = alphaBeta(logicBoard, depth, -1000000, 1000000, MAX_PLAYER);
	logicBoard->setMarkO(bestMove.x, bestMove.y);
	std::cout << "Number of created nodes in game tree: " << nodeCounter << std::endl;
	return bestMove;
}

Move PlayerAI::miniMax(LogicBoard *logicBoard, int depth, int player) {
	nodeCounter++;
	int result = logicBoard->checkWin();

	
	if (result == AI_PLAYER) {
		return Move(10);
	}
	else if (result == HUMAN_PLAYER) {
		return Move(-10);
	}
	else if (result == TIE){
		return Move(0);
	}
	else if (depth == 0) {
		return Move(0);
	}

	std::vector<Move> moves;

	for (int x = 0; x < logicBoard->checkSize(); x++) {
		for (int y = 0; y < logicBoard->checkSize(); y++) {
			if (logicBoard->checkSquare(x, y) == NO_MARK) {
				Move move;
				move.x = x;
				move.y = y;
				
				// tura AI
				if (player == AI_PLAYER) {
					logicBoard->setMarkO(x, y);
					move.score = miniMax(logicBoard, depth - 1, HUMAN_PLAYER).score;
				}
				// tura gracza
				else if (player == HUMAN_PLAYER) {
					logicBoard->setMarkX(x, y);
					move.score = miniMax(logicBoard, depth - 1, AI_PLAYER).score;
				}

				moves.push_back(move);

				logicBoard->clearMark(x, y);
			}
		}
	}

	int bestMove = 0;
	if (player == AI_PLAYER) {
		int bestScore = -100000;
		for (int i = 0; i < moves.size(); i++) {
			if (moves[i].score > bestScore) {
				bestMove = i;
				bestScore = moves[i].score;
			}
		}
	}
	else {
		int bestScore = 100000;
		for (int i = 0; i < moves.size(); i++) {
			if (moves[i].score < bestScore) {
				bestMove = i;
				bestScore = moves[i].score;
			}
		}
	}

	return moves[bestMove];
}

Move PlayerAI::alphaBeta(
	LogicBoard *logicBoard, int depth,
	int alpha, int beta, 
	int player) {

	nodeCounter++;
	int v;
	int result = logicBoard->checkWin();

	
	if (result == MAX_PLAYER) {
		return Move(10);
	}
	else if (result == MIN_PLAYER) {
		return Move(-10);
	}
	else if (result == TIE) {
		return Move(0);
	}
	else if (depth == 1) {
		return Move(0);
	}
	

	std::vector<Move> moves;
	
	if (player == MAX_PLAYER) v = -100000;
	else if (player == MIN_PLAYER) v = 100000;

	for (int x = 0; x < logicBoard->checkSize(); x++) {
		for (int y = 0; y < logicBoard->checkSize(); y++) {
			if (logicBoard->checkSquare(x, y) == NO_MARK) {
				Move move;
				move.x = x;
				move.y = y;

				// tura AI
				if (player == MAX_PLAYER) {
					logicBoard->setMarkO(x, y);
					move.score = alphaBeta(logicBoard, depth - 1, alpha, beta, MIN_PLAYER).score;
					v = std::max(v, move.score);
					alpha = std::max(alpha, v);
				}
				// tura gracza
				else if (player == MIN_PLAYER) {
					logicBoard->setMarkX(x, y);
					move.score = alphaBeta(logicBoard, depth - 1, alpha, beta, MAX_PLAYER).score;
					v = std::min(v, move.score);
					beta = std::min(beta, v);
				}

				moves.push_back(move);
				logicBoard->clearMark(x, y);
				if (beta <= alpha) break;
			}
		}
	}
	
	int bestMove = 0;
	int movesSize = moves.size();
	if (player == MAX_PLAYER) {
		int bestScore = -100000;
		for (int i = 0; i < movesSize; i++) {
			if (moves[i].score > bestScore) {
				bestMove = i;
				bestScore = moves[i].score;
			}
		}
	}
	else {
		int bestScore = 100000;
		for (int i = 0; i < movesSize; i++) {
			if (moves[i].score < bestScore) {
				bestMove = i;
				bestScore = moves[i].score;
			}
		}
	}

	return moves[bestMove];
}