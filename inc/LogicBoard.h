
#include <iostream>

using namespace std;

class Player {
public:
	int x;
	int y;
};

class LogicBoard {
public:
	LogicBoard(int size, int winRow);
	LogicBoard() {
		_size = 0;
		_winRow = 0;
	}
	~LogicBoard();

	void setMarkX(int x, int y);
	void setMarkO(int x, int y);
	void clearMark(int x, int y);

	int checkSquare(int x, int y);

	int checkWin();



	void init(int size, int winRow);

	int checkSize() {
		return _size;
	}

	int checkWinRow() {
		return _winRow;
	}

	void display();
	void displayWinningCombinationO();
	void displayWinningCombinationX();

	void clear();

	void clearWinningCombinationO();
	void clearWinningCombinationX();

	Player *_winningCombinationO;
	Player *_winningCombinationX;

private:
	int _size;
	int _winRow;
	int **_arr = nullptr;
	
};


//***************************************************************************************************************
//          
//***************************************************************************************************************
LogicBoard::LogicBoard(int size, int winRow) {
	init(size, winRow);
}

LogicBoard::~LogicBoard() {
	for (int i = 0; i < _size; i++) {
		delete[] _arr[i];
	}
	delete[] _arr;
	delete[] _winningCombinationO;
	delete[] _winningCombinationX;
}

void LogicBoard::setMarkX(int x, int y) {
	if (x < _size && x >= 0 && y < _size && y >= 0) {
		_arr[x][y] = 1;
	}
}

void LogicBoard::setMarkO(int x, int y) {
	if (x < _size && x >= 0 && y < _size && y >= 0) {
		_arr[x][y] = 2;
	}
}

void LogicBoard::clearMark(int x, int y) {
	_arr[x][y] = 0;
}

void LogicBoard::init(int size, int winRow) {
	_size = size;
	_winRow = winRow;

	_arr = new int*[_size];
	for (int i = 0; i < _size; i++) {
		_arr[i] = new int[_size];
	}

	_winningCombinationO = new  Player[_winRow];
	_winningCombinationX = new  Player[_winRow];

	clear();
}

void LogicBoard::clear() {
	for (int i = 0; i < _size; i++) {
		for (int j = 0; j < _size; j++) {
			_arr[i][j] = 0;
		}
	}
}

void LogicBoard::clearWinningCombinationO() {
	for (int i = 0; i < _winRow; i++) {
		_winningCombinationO[i].x = 0;
		_winningCombinationO[i].y = 0;
	}
}

void LogicBoard::clearWinningCombinationX() {
	for (int i = 0; i < _winRow; i++) {
		_winningCombinationX[i].x = 0;
		_winningCombinationX[i].y = 0;
	}
}

int LogicBoard::checkSquare(int x, int y) {
	return _arr[x][y];
}


int LogicBoard::checkWin() {
	const int X_MARK = 1;
	const int O_MARK = 2;
	const int NO_MARK = 0;

	int playerPointsX = 0;
	int playerPointsO = 0;

	int horizontalPointsX = 0;
	int horizontalPointsO = 0;

	int verticalPointsX = 0;
	int verticalPointsO = 0;

	int diagonalRightPointsX = 0;
	int diagonalRightPointsO = 0;

	int diagonalLeftPointsX = 0;
	int diagonalLeftPointsO = 0;

	int x = 0;

	for (int y = 0; y < _size; y++) {
		for (int x = 0; x < _size; x++) {
			if (_arr[x][y] == X_MARK) {
				_winningCombinationX[horizontalPointsX].x = x;
				_winningCombinationX[horizontalPointsX].y = y;
				horizontalPointsX++;
				if (horizontalPointsO != 0) horizontalPointsO=0;
			}
			else if (_arr[x][y] == O_MARK) {
				
				_winningCombinationO[horizontalPointsO].x = x;
				_winningCombinationO[horizontalPointsO].y = y;
				horizontalPointsO++;
				if (horizontalPointsX != 0) horizontalPointsX=0;
			}
			else if (_arr[x][y] == NO_MARK) {
				if (horizontalPointsO != 0) horizontalPointsO=0;
				if (horizontalPointsX != 0) horizontalPointsX=0;
			}
			if (horizontalPointsX == _winRow && horizontalPointsO <= _size - _winRow) return X_MARK;
			else if (horizontalPointsO == _winRow && horizontalPointsX <= _size - _winRow) return O_MARK;

			if (_arr[y][x] == X_MARK) {
				
				_winningCombinationX[verticalPointsX].x = y;
				_winningCombinationX[verticalPointsX].y = x;
				verticalPointsX++;
				if (verticalPointsO != 0) verticalPointsO=0;
			}
			else if (_arr[y][x] == O_MARK) {
				
				_winningCombinationO[verticalPointsO].x = y;
				_winningCombinationO[verticalPointsO].y = x;
				verticalPointsO++;
				if (verticalPointsX != 0) verticalPointsX=0;
			}
			else if (_arr[y][x] == NO_MARK) {
				if (verticalPointsO != 0) verticalPointsO=0;
				if (verticalPointsX != 0) verticalPointsX=0;
			}
			if (verticalPointsX == _winRow && verticalPointsO <= _size - _winRow) return X_MARK;
			else if (verticalPointsO == _winRow && verticalPointsX <= _size - _winRow) return O_MARK;
		}
		horizontalPointsX = 0;
		horizontalPointsO = 0;
		verticalPointsX = 0;
		verticalPointsO = 0;
		clearWinningCombinationO();
		clearWinningCombinationX();
	}



	for (int x = 0; x <= _size - _winRow; x++) {
		for (int i = 0; i < _size - x; i++) {
			if (_arr[i + x][i] == X_MARK) {
				
				_winningCombinationX[diagonalRightPointsX].x = i + x;
				_winningCombinationX[diagonalRightPointsX].y = i;
				diagonalRightPointsX++;
				if (diagonalRightPointsO != 0) diagonalRightPointsO=0;
			}
			else if (_arr[i + x][i] == O_MARK) {
				
				_winningCombinationO[diagonalRightPointsO].x = i + x;
				_winningCombinationO[diagonalRightPointsO].y = i;
				diagonalRightPointsO++;
				if (diagonalRightPointsX != 0) diagonalRightPointsX=0;
			}
			else if (_arr[i + x][i] == NO_MARK) {
				if (diagonalRightPointsO != 0) diagonalRightPointsO=0;
				if (diagonalRightPointsX != 0) diagonalRightPointsX=0;
			}
			if (diagonalRightPointsX == _winRow && diagonalRightPointsO <= _size - _winRow) return X_MARK;
			else if (diagonalRightPointsO == _winRow && diagonalRightPointsX <= _size - _winRow) return O_MARK;

			if (_arr[_size - 1 - i - x][i] == X_MARK) {
				
				_winningCombinationX[diagonalLeftPointsX].x = _size - 1 - i - x;
				_winningCombinationX[diagonalLeftPointsX].y = i;
				diagonalLeftPointsX++;
				if (diagonalLeftPointsO != 0) diagonalLeftPointsO=0;
			}
			else if (_arr[_size - 1 - i - x][i] == O_MARK) {
				
				_winningCombinationO[diagonalLeftPointsO].x = _size - 1 - i - x;
				_winningCombinationO[diagonalLeftPointsO].y = i;
				diagonalLeftPointsO++;
				if (diagonalLeftPointsX != 0) diagonalLeftPointsX=0;
			}
			else if (_arr[_size - 1 - i - x][i] == NO_MARK) {
				if (diagonalLeftPointsO != 0) diagonalLeftPointsO=0;
				if (diagonalLeftPointsX != 0) diagonalLeftPointsX=0;
			}
			if (diagonalLeftPointsX == _winRow && diagonalLeftPointsO <= _size - _winRow) return X_MARK;
			else if (diagonalLeftPointsO == _winRow && diagonalLeftPointsX <= _size - _winRow) return O_MARK;
		}
		diagonalRightPointsX = 0;
		diagonalRightPointsO = 0;
		diagonalLeftPointsX = 0;
		diagonalLeftPointsO = 0;
		clearWinningCombinationO();
		clearWinningCombinationX();
	}
	for (int x = 0; x < _size; x++) {
		for (int y = 0; y < _size; y++) {
			if (_arr[x][y] == 0) return 0;
		}
	}
	
	return -1;
}

void LogicBoard::display() {
	for (int j = 0; j < _size; j++) {
		for (int i = 0; i < _size; i++) {
			cout << "|" << _arr[i][j];
		}
		cout << "|" << endl;
	}
}

void LogicBoard::displayWinningCombinationO() {
	for (int i = 0; i < _winRow; i++) {
		cout << i << ". x: " << _winningCombinationO[i].x << " y: " << _winningCombinationO[i].y << endl;
	}
}

void LogicBoard::displayWinningCombinationX() {
	for (int i = 0; i < _winRow; i++) {
		cout << i << ". x: " << _winningCombinationX[i].x << " y: " << _winningCombinationX[i].y << endl;
	}
}