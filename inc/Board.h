
#include <string>
#include <SDL.h>
#include "PlayerAI.h"
#include <iostream>
#include <cmath>
#include <chrono>

using namespace std;



class Board {
public:
	Board(const string &title, 
		int width, int height, 
		int tableSize, int squareSize, 
		int winRow, int playersTurn,
		int difficultyLevel); //wywoluje funkcje init(), tworzy okno
	~Board(); //zamyka i czystci okno

	LogicBoard _logicBoard;
	
	void detectEvent(); //interakcja z oknem
	
	bool isClicked() {
		return _isClicked;
	}

	void resetClick() {
		_isClicked = false;
	}

	bool isReset() {
		return _isReset;
	}

	void setReset() {
		_isReset = true;
	}

	void clear();
	void createTable(); //tworzy kwadratowa plansze o zadanym rozmiarze
	void drawPlayerIndicatorSquare();

	void drawX(int x, int y, int R, int G, int B);
	void drawPlayerX();
	void drawO(int x, int y, int R, int G, int B);
	void drawPlayerO();

	void drawPlayerIndicatorX();
	void drawPlayerIndicatorO();

	void drawWinTableX();
	void drawWinTableO();
	void drawTieTable();

	void drawRedSquare(int x1, int y1);
	void drawBlueSquare(int x1, int y1);

	bool whichPlayer() {
		return _playersTurn;
	}

	void enableAI(bool n) {
		_AIenabled = n;
	}

	void SDL_RenderDrawLineScale(
		SDL_Renderer *renderer,
		int x1, int y1,
		int x2, int y2,
		float scale);

	inline bool isClosed() const { //sprawdzanie czy okno powinno byc zamkniete
		return _closed;
	}

	void present() {
		SDL_RenderPresent(_renderer); //wyswietla wszystkie zmiany
	}

private:
	bool init();
	
	SDL_Rect _mainBlackFrame;
	SDL_Rect _mainRect;

	SDL_Rect _playerIndicatorRectFrame;
	SDL_Rect _playerIndicatorRect;

	int _squareSize = 100;
	int _playerIndicatorSquareSize = 50;
	int _tableSize = 3;
	int _winRow = 3;

	bool _playersTurn = 1;
	bool _AIenabled = false;

	string _title; //tytul na gornym pasku
	int _width = 800;
	int _height = 600;

	bool _closed = false; //flaga zamkniecia
	bool _isClicked = false;
	bool _isReset = false;

	int _difficultyLevel = 0;

	Player _playerX;
	Player _playerO;
	PlayerAI _playerAI;
	bool switchPlayersTurn();

	SDL_Window *_window = nullptr; //tak na poczatek

	SDL_Renderer *_renderer = nullptr;

};

//*******************************************************************************************************************************

//*******************************************************************************************************************************

Board::Board(const string &title, int width, int height, int tableSize, int squareSize, int winRow, int playersTurn, int difficultyLevel) :
	_title(title), _width(width), _height(height), _tableSize(tableSize), _squareSize(squareSize), _winRow(winRow), _playersTurn(playersTurn), _difficultyLevel(difficultyLevel)
{
	_playerAI._difficultyLevel = _difficultyLevel;
	_closed = !init(); //generalna inicjalizacja
	
}


Board::~Board() {
	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_window);
	
}

bool Board::init() {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) { //tylko grafika
		cerr << "SDL video initialization failed" << endl;
		return false;
	}

	_window = SDL_CreateWindow( //inicjalizacja okna
		_title.c_str(), //konwersja stringa na standard C
		SDL_WINDOWPOS_CENTERED, //na srodku ekranu
		SDL_WINDOWPOS_CENTERED,
		_width, _height,
		SDL_WINDOW_RESIZABLE //mozliwosc rozszerzania okna
		);

	if (_window == nullptr) { //kontorla inicjalizacji okna
		cerr << "SDL failed to create window" << endl;
		return false;
	}


	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED); //inicjalizacja renderera

	if (_renderer == nullptr) { //kontorla inicjalizacji renderera
		cerr << "SDL failed to render in window" << endl;
		return false;
	}

	_logicBoard.init(_tableSize, _winRow);

	return true; //inicjalizacja sie powiodla
}

void Board::detectEvent() {
	SDL_Event event;
	
	// zakomentowac jesli bez AI ->
	if (_AIenabled) {
		if (_playersTurn == 0 && _isReset == false) {

			cout << "I'm thinking..." << endl;
			chrono::steady_clock::time_point _start(chrono::steady_clock::now());
			Move temp = _playerAI.placeMark(&_logicBoard);
			chrono::steady_clock::time_point _end(chrono::steady_clock::now());
			//long long ftime = std::chrono::duration_cast<std::chrono::milliseconds>(time).count();
			cout << "Done after: " << chrono::duration_cast<chrono::duration<double>>(_end - _start).count() << " seconds" << endl;
			cout << "Your turn." << endl;

			_playerO.x = temp.x;
			_playerO.y = temp.y;

			_isClicked = true;
			_playersTurn = 1;
		}
	}
	// <-

	if (SDL_PollEvent(&event)) { //funckja z biblioteki wykrywajaca interakcje z oknem
		
		int x = 0;
		int y = 0;

		int x0 = 0;
		int y0 = 0;
		
		switch (event.type) {
		case SDL_QUIT: //przycisk X na gornym pasku
			_closed = true;
			break;

		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
			case SDLK_ESCAPE:
				_closed = true;
				system("cls"); // NO WIEM
				break;
			case SDLK_c:
				cout << endl << "NEW GAME" << endl;
				_isReset = false;
				_playersTurn = !_playersTurn;
				clear();
				
				if (whichPlayer() == 1) {
					drawPlayerIndicatorX();
				}
				else {
					drawPlayerIndicatorO();
				}
			default:
				break;
			}
			break;

		case SDL_MOUSEMOTION:
			//cout << event.motion.x << ", " << event.motion.y << endl;
			break;

		case SDL_MOUSEBUTTONDOWN:

			x = event.motion.x;
			y = event.motion.y;

			x0 = _mainRect.x;
			y0 = _mainRect.y;

			//cout << "MOUSE ACTIVE!" << "(X,Y): " << x << ", " << y <<endl;
			//cout << "           " << "(X0,Y0): " << x0 << ", " << y0 << endl;
			
			

			for (int i = 0; i < _tableSize; i++) {
				for (int j = 0; j < _tableSize; j++) {
					if (x > x0 + _squareSize * i && x < x0 + _squareSize * i + _squareSize) {
						if (y > y0 + _squareSize * j && y < y0 + _squareSize * j + _squareSize) {
							if (_logicBoard.checkSquare(i, j) != 1 && _logicBoard.checkSquare(i, j) != 2) {
								switch (_playersTurn) {
								case 0:
									// bez AI
									if (!_AIenabled) {
										if (_isReset == false) {
											_playerO.x = i;
											_playerO.y = j;
											//_logicBoard.setMarkO(i, j);
											_isClicked = true;
											_playersTurn = 1;
										}
									}
									break;
								case 1:
									if (_isReset == false) {
										_playerX.x = i;
										_playerX.y = j;
										//_logicBoard.setMarkX(i, j);
										_isClicked = true;
										_playersTurn = 0;
									}
									break;
								default:
									break;
								}
								//switchPlayersTurn();
							}
						}
					}
				}
			}

			break;

		default:
			break;
		}
	}
}

void Board::clear() {
	SDL_SetRenderDrawColor(_renderer, 180, 180, 180, 255); //wybiera kolor 
	SDL_RenderClear(_renderer); //ustawia kolor

	_logicBoard.clear();
	createTable();

	//SDL_RenderPresent(_renderer); //wyswietla wszystkie zmiany
}

void Board::createTable() {
	int size = _squareSize * _tableSize;	//rozmiar komórki x ilosć komórek
	int lineWidth = 3; 						//grubość rysowanej linii
	
	// mainBlackFrame

	_mainBlackFrame.h = size + 2 * lineWidth;
	_mainBlackFrame.w = size + 2 * lineWidth;
	_mainBlackFrame.x = _width / 2 - _mainBlackFrame.w / 2;
	_mainBlackFrame.y = _height / 2 - _mainBlackFrame.h / 2;

	SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255); //wybiera kolor 
	SDL_RenderFillRect(_renderer, &_mainBlackFrame); //ustawia kolor

	//mainRect

	_mainRect.h = size;
	_mainRect.w = size;
	_mainRect.x = _width / 2 - _mainRect.w / 2;
	_mainRect.y = _height / 2 - _mainRect.h / 2;

	SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255); //wybiera kolor 
	SDL_RenderFillRect(_renderer, &_mainRect); //ustawia kolor

	for (int i = 1; i < _tableSize; i++) {
		int x1 = _mainRect.x + _squareSize * i;
		int y1 = _mainRect.y;

		int x2 = _mainRect.x + _squareSize * i;
		int y2 = _mainRect.y + _mainRect.h;

		SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255); //wybiera kolor 
		SDL_RenderDrawLineScale(_renderer, x1, y1, x2, y2, 2.0);

		x1 = _mainRect.x;
		y1 = _mainRect.y + _squareSize * i;

		x2 = _mainRect.x + _mainRect.w;
		y2 = _mainRect.y + _squareSize * i;
		SDL_RenderDrawLineScale(_renderer, x1, y1, x2, y2, 2.0);
	}

	drawPlayerIndicatorSquare();
}

void Board::drawWinTableX() {
	for (int i = 0; i < _tableSize; i++) {
		int x = _logicBoard._winningCombinationX[i].x;
		int y = _logicBoard._winningCombinationX[i].y;
		drawX(x, y, 0, 200, 0);
	}
	drawPlayerIndicatorSquare();
	_logicBoard.displayWinningCombinationX();
}

void Board::drawRedSquare(int x1, int y1) {
	SDL_Rect redRect;

	redRect.x = _mainRect.x + _squareSize * x1;
	redRect.y = _mainRect.y + _squareSize * y1;

	redRect.h = _squareSize;
	redRect.w = _squareSize;

	SDL_SetRenderDrawColor(_renderer, 250, 200, 200, 255);
	SDL_RenderFillRect(_renderer, &redRect);
}



void Board::drawWinTableO() {
	for (int i = 0; i < _tableSize; i++) {
		int x = _logicBoard._winningCombinationO[i].x;
		int y = _logicBoard._winningCombinationO[i].y;
		drawO(x, y, 0, 200, 0);
	}
	drawPlayerIndicatorSquare();
	_logicBoard.displayWinningCombinationX();
}

void Board::drawBlueSquare(int x1, int y1) {
	SDL_Rect blueRect;

	blueRect.x = _mainRect.x + _squareSize * x1;
	blueRect.y = _mainRect.y + _squareSize * y1;

	blueRect.h = _squareSize;
	blueRect.w = _squareSize;

	SDL_SetRenderDrawColor(_renderer, 200, 200, 250, 255);
	SDL_RenderFillRect(_renderer, &blueRect);
}

void Board::drawTieTable() {
	int size = _squareSize * _tableSize;
	int lineWidth = 3;

	//mainRect

	_mainRect.h = size;
	_mainRect.w = size;
	_mainRect.x = _width / 2 - _mainRect.w / 2;
	_mainRect.y = _height / 2 - _mainRect.h / 2;

	SDL_SetRenderDrawColor(_renderer, 200, 200, 200, 255); //wybiera kolor 
	SDL_RenderFillRect(_renderer, &_mainRect); //ustawia kolor

	//siatka	
	for (int i = 1; i < _tableSize; i++) {
		int x1 = _mainRect.x + _squareSize * i;
		int y1 = _mainRect.y;

		int x2 = _mainRect.x + _squareSize * i;
		int y2 = _mainRect.y + _mainRect.h;

		SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255); //wybiera kolor 
		SDL_RenderDrawLineScale(_renderer, x1, y1, x2, y2, 2.0);

		x1 = _mainRect.x;
		y1 = _mainRect.y + _squareSize * i;

		x2 = _mainRect.x + _mainRect.w;
		y2 = _mainRect.y + _squareSize * i;
		SDL_RenderDrawLineScale(_renderer, x1, y1, x2, y2, 2.0);
	}

	drawPlayerIndicatorSquare();
}

void Board::drawPlayerIndicatorSquare() {
	_playerIndicatorRect.h = _playerIndicatorSquareSize;
	_playerIndicatorRect.w = _playerIndicatorSquareSize;
	_playerIndicatorRect.x = _width / 2 + _mainBlackFrame.w / 2 + 20;
	_playerIndicatorRect.y = _height / 2 - _playerIndicatorRect.w / 2;

	_playerIndicatorRectFrame.h = _playerIndicatorRect.h + 8;
	_playerIndicatorRectFrame.w = _playerIndicatorRect.w + 8;
	_playerIndicatorRectFrame.x = _playerIndicatorRect.x - 4;
	_playerIndicatorRectFrame.y = _playerIndicatorRect.y - 4;

	SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(_renderer, &_playerIndicatorRectFrame);

	SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(_renderer, &_playerIndicatorRect);
}

void Board::drawX(int x, int y, int R, int G, int B) {
	if (x == _tableSize) {
		cerr << "Wrong x index." << endl;
	}
	if (y == _tableSize) {
		cerr << "Wrong y index." << endl;
	}

	_logicBoard.setMarkX(x, y);

	int offset = 0.1 * _squareSize;
	SDL_SetRenderDrawColor(_renderer, R, G, B, 255); //wybiera kolor 

	int x1 = _mainRect.x + x * _squareSize + offset;
	int y1 = _mainRect.y + y * _squareSize + offset;

	int x2 = x1 + _squareSize - 2 * offset;
	int y2 = y1 + _squareSize - 2 * offset;

	SDL_RenderDrawLine(_renderer, x1, y1, x2, y2);

	x1 = _mainRect.x + x * _squareSize + _squareSize - offset;
	y1 = _mainRect.y + y * _squareSize + offset;

	x2 = x1 - _squareSize + 2 * offset;
	y2 = y1 + _squareSize - 2 * offset;

	SDL_RenderDrawLine(_renderer, x1, y1, x2, y2);
	
}

void Board::drawPlayerIndicatorX() {

	int offset = 0.1 * _playerIndicatorSquareSize;
	SDL_SetRenderDrawColor(_renderer, 200, 0, 0, 255); //wybiera kolor 

	int x1 = _playerIndicatorRect.x + offset;
	int y1 = _playerIndicatorRect.y + offset;

	int x2 = x1 + _playerIndicatorSquareSize - 2 * offset;
	int y2 = y1 + _playerIndicatorSquareSize - 2 * offset;

	SDL_RenderDrawLine(_renderer, x1, y1, x2, y2);

	x1 = _playerIndicatorRect.x + _playerIndicatorSquareSize - offset;
	y1 = _playerIndicatorRect.y + offset;

	x2 = x1 - _playerIndicatorSquareSize + 2 * offset;
	y2 = y1 + _playerIndicatorSquareSize - 2 * offset;

	SDL_RenderDrawLine(_renderer, x1, y1, x2, y2);
}


void Board::drawPlayerX() {
	drawX(_playerX.x, _playerX.y, 2000, 0, 0);
	drawPlayerIndicatorSquare();
	drawPlayerIndicatorO();
}

void Board::drawO(int x, int y, int R, int G, int B) {
	if (x == _tableSize) {
		cerr << "Wrong x index." << endl;
	}
	if (y == _tableSize) {
		cerr << "Wrong y index." << endl;
	}

	_logicBoard.setMarkO(x, y);

	int offset = 0.1 * _squareSize;
	int radius = _squareSize / 2 - offset;

	int x0 = _mainRect.x + x * _squareSize + 0.5 * _squareSize;
	int y0 = _mainRect.y + y * _squareSize + 0.5 * _squareSize;

	int x1 = radius - 1;
	int y1 = 0;
	int dx = 1;
	int dy = 1;
	int err = dx - (radius << 1);

	SDL_SetRenderDrawColor(_renderer, R, G, B, 255); //wybiera kolor 

	while (x1 >= y1)
	{
		SDL_RenderDrawPoint(_renderer, x0 + x1, y0 + y1);
		SDL_RenderDrawPoint(_renderer, x0 + y1, y0 + x1);
		SDL_RenderDrawPoint(_renderer, x0 - y1, y0 + x1);
		SDL_RenderDrawPoint(_renderer, x0 - x1, y0 + y1);
		SDL_RenderDrawPoint(_renderer, x0 - x1, y0 - y1);
		SDL_RenderDrawPoint(_renderer, x0 - y1, y0 - x1);
		SDL_RenderDrawPoint(_renderer, x0 + y1, y0 - x1);
		SDL_RenderDrawPoint(_renderer, x0 + x1, y0 - y1);

		if (err <= 0)
		{
			y1++;
			err += dy;
			dy += 2;
		}

		if (err > 0)
		{
			x1--;
			dx += 2;
			err += dx - (radius << 1);
		}
	}

}

void Board::drawPlayerIndicatorO() {
	
	int offset = 0.1 * _playerIndicatorSquareSize;
	int radius = _playerIndicatorSquareSize / 2 - offset;

	int x0 = _playerIndicatorRect.x + _playerIndicatorSquareSize / 2;
	int y0 = _playerIndicatorRect.y + _playerIndicatorSquareSize / 2;

	int x1 = radius - 1;
	int y1 = 0;
	int dx = 1;
	int dy = 1;
	int err = dx - (radius << 1);

	SDL_SetRenderDrawColor(_renderer, 0, 0, 200, 255); //wybiera kolor 

	while (x1 >= y1)
	{
		SDL_RenderDrawPoint(_renderer, x0 + x1, y0 + y1);
		SDL_RenderDrawPoint(_renderer, x0 + y1, y0 + x1);
		SDL_RenderDrawPoint(_renderer, x0 - y1, y0 + x1);
		SDL_RenderDrawPoint(_renderer, x0 - x1, y0 + y1);
		SDL_RenderDrawPoint(_renderer, x0 - x1, y0 - y1);
		SDL_RenderDrawPoint(_renderer, x0 - y1, y0 - x1);
		SDL_RenderDrawPoint(_renderer, x0 + y1, y0 - x1);
		SDL_RenderDrawPoint(_renderer, x0 + x1, y0 - y1);

		if (err <= 0)
		{
			y1++;
			err += dy;
			dy += 2;
		}

		if (err > 0)
		{
			x1--;
			dx += 2;
			err += dx - (radius << 1);
		}
	}
}

void Board::drawPlayerO() {
	drawO(_playerO.x, _playerO.y, 0, 0, 200);
	drawPlayerIndicatorSquare();
	drawPlayerIndicatorX();
}


void Board::SDL_RenderDrawLineScale(
	SDL_Renderer *renderer, 
	int x1, int y1, 
	int x2, int y2, 
	float scale) {

	x1 /= scale;
	y1 /= scale;
	x2 /= scale;
	y2 /= scale;

	SDL_RenderSetScale(renderer, scale, scale);
	SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
	SDL_RenderSetScale(renderer, 1.0, 1.0);
}

bool Board::switchPlayersTurn() {
	_playersTurn = !_playersTurn;
	return _playersTurn;
}




