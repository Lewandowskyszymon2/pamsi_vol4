CPP = g++ -pedantic -std=c++11 -c -g -Iinc 
LDFLAGS_LINUX = -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer

start: gra
	./a.out

gra: obj object_main
	g++ obj/main.o ${LDFLAGS_LINUX}
	
obj:
	mkdir obj

object_main: src/main.cpp
	${CPP} src/main.cpp -o obj/main.o 


clear:
	rm -rf obj