
#include <iostream>
#include <SDL2/SDL.h>  
//#include <SDL_mixer.h>
#include "Board.h"


using namespace std;


int main(int argc, char **argv) 
{
	int tableSize = -1;
	int winRow = tableSize + 1;
	char ifAI = 's';
	bool AIenable;
	int difficultyLevel = 0;
	int playerTurn; 
	char whichPlayer = 'a';
	int FHD_Y = 1080;
	char whichOption = 'a';
	int squareSize, width, height;
	/*-------------------------AUDIO------------------------------------*/
	/*------------------------------------------------------------------*/
	// Initialize SDL.
	/* if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return 1;
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	
	Mix_Music *MenuBackgroundMusic = Mix_LoadMUS("sounds/MenuBackground.wav");
	*/
	/*------------------------------------------------------------------*/
	/*------------------------------------------------------------------*/


	bool exit = false;
	
	while (!exit) {
	//	Mix_PlayMusic(MenuBackgroundMusic, -1);
		cout<<endl<<"Press any key to run the game."<<endl<<"press ESC and ENTER to exit."<<endl;
		int temporar;
		temporar=getchar();
		if (temporar == 27) {
			exit = true;
			break;
		}
		if (temporar == 113) {
			tableSize=3;
			winRow=3;
			AIenable = true;
			difficultyLevel=4;
			playerTurn = 1;
			FHD_Y = 720;

			
			squareSize = (FHD_Y / tableSize) / 2;
			width = tableSize * squareSize + 2 * squareSize;
			height = tableSize * squareSize + 2 * squareSize;
		}else
		{
			cin.clear();
			cout<<"Choose how many rows and columns shall be on the board. \n (board needs to be square so put in one value greater or equal than 2)"<<endl;

			
			while (tableSize < 2) {
				while (cout << " "  && !(cin >> tableSize)) {
					cerr << "Wrong data type. Try again." << endl;
					cin.clear();
					cin.ignore(999, '\n');
				}
				if (tableSize < 2) cerr << "Sorry. Value must be greater or equal to 2." << endl;
			}

			cout << "Please, input the number of symbols in a row needed to win a game." << endl;
			
			while (winRow > tableSize || winRow < 2 ) {
				while (cout << " "  && !(cin >> winRow)) {
					cerr << "Wrong data type. Try again." << endl;
					cin.clear();
					cin.ignore(999, '\n');
				}
				if (winRow > tableSize) cerr << "r must be less or equal to x." << endl;
				if (winRow < 2) cerr << "r must be greater or equal to 2" << endl;
			}

			
			cout << endl << "Do you want to play against AI?" << endl;
			while (ifAI != 'y' && ifAI != 'n') {
				cout << "(y/n): "; cin >> ifAI;

				if (ifAI == 'y') AIenable = true;
				else if (ifAI == 'n') AIenable = false;
				else cerr << "Wrong answer, try again" << endl;
			}

			if (AIenable) {
				cout << endl << "Chose AI difficulty level:" << endl;
				cout << "1 - EASY" << endl;
				cout << "2 - MEDIUM" << endl;
				cout << "3 - HARD" << endl;
				cout << "4 - IMPOSSIBLE" << endl;
				while ((difficultyLevel < 1 || difficultyLevel > 4) && cout << "Chose difficulty level: " && !(cin >> difficultyLevel)) {
					cerr << "Wrong data type. Try again." << endl;
					cin.clear();
					cin.ignore(999, '\n');
					
					if (difficultyLevel < 1 || difficultyLevel > 4)
						cerr << "Wrong choice. Try again." << endl;
			}
			}

			
			while (whichPlayer != 'x' && whichPlayer != 'o') {
				cout << endl << "Who starts the game?" << endl;
				if (AIenable) {
					cout << "x - you" << endl;
					cout << "o - AI" << endl;
				}
				else {
					cout << "x - Player X" << endl;
					cout << "o - Player O" << endl;
				}
				cout << "Chose starting player and press ENTER: "; cin >> whichPlayer;
				if (whichPlayer == 'x') playerTurn = 1;
				else if (whichPlayer == 'o') playerTurn = 0;
				else cerr << "Wrong choice, try again." << endl;
			}

			while (whichOption != 'F' && whichOption != 'B') {
				cout << endl << "what's your display resolution?" << endl;
				
					cout << " F-full HD(1080p)" << endl;
					cout << " B-basic HD(720p)" << endl;
				
				cout << "Chose your resolution (B/F) "; cin >> whichOption;
				if (whichOption == 'B') FHD_Y = 720;
				else if (whichOption == 'F') FHD_Y = 1080;
				else cerr << "Wrong choice, try again." << endl;
			}
			

			squareSize = (FHD_Y / tableSize) / 2;

			if (squareSize <= 80) width = tableSize * squareSize + 2 * (squareSize + 80);
			else width = tableSize * squareSize + 2 * squareSize;

			height = tableSize * squareSize + 2 * squareSize;
		}
		//***********************************************************************************************************
		Board board("Game", width, height, tableSize, squareSize, winRow, playerTurn, difficultyLevel);

		

		board.clear();

		int result = 0;
		int moveCount = 0;
		board.enableAI(AIenable);
		cout << endl << "GAME HAS STARTED" << endl;
		if (board.whichPlayer() == 1) {
			cout << "X";
			board.drawPlayerIndicatorX();
		}
		else {
			board.drawPlayerIndicatorO();
			cout << "O";
		}
		cout << "'s TURN" << endl;
		
		while (!board.isClosed()) {
			
			board.detectEvent();

			if (board.isClicked() && !board.isReset()) {
				switch (board.whichPlayer()) {
				case 0:
					board.drawPlayerX();
					moveCount++;
					cout << endl << "O's TURN" << endl;
					break;
				case 1:
					board.drawPlayerO();
					moveCount++;
					cout << endl << "X's TURN" << endl;
					break;
				}
				
				board._logicBoard.display();
				result = board._logicBoard.checkWin();
				//cout << "RESULT: " << result << endl;
				switch (result) {
				case -1:
					cout << endl << "DRAW" << endl;
					break;
				case 0:
					//cout << "NO WINNER" << endl;
					break;
				case 1:
					cout << endl << "X WON" << endl;
					break;
				case 2:
					cout << endl << "O WON" << endl;
					break;
				default:
					break;
				}
				

				board.resetClick();
			}

			if (result == 0) board.present();
			else if (result == -1) {
				cout << endl << "press c to reset the game board" << endl;
				cout << "or ESC to close the window" << endl;
				board.drawTieTable();
				board.present();
				result = 0;
				moveCount = 0;
				board.setReset();
			}
			else if (result == 1 || result == 2) {
				if (result == 1) {
					board.drawWinTableX();
				}
				else if (result == 2) {
					board.drawWinTableO();
				}
				cout << endl << "press c to reset the game board" << endl;
				cout << "or ESC to close the window" << endl;
				board.present();
				result = 0;
				moveCount = 0;
				board.setReset();
			}}

    }
	SDL_Quit();
	cout << "Thank you for playing" << endl;
	getchar();
    return 0;
}